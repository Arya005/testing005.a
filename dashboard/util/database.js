const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let dbObj;

const mongoConnect = callback => {
    MongoClient.connect('mongodb://172.31.9.144:27017/?readPreference=primary&directConnection=true&ssl=false')
    .then((client) => {
        console.log('connected');
        dbObj = client.db('abc');
        callback();
    }).catch((err) => {
        console.log(err);
        throw err;
    });
}

const getDb = () => {
    if(dbObj) {
        return dbObj;
    }
    throw 'No DB found!';
}
module.exports = {
    mongoConnect,
    getDb
};
// module.exports = getDb;