const Report = require('../models/report');


exports.getReportData = (req,res,next) => {
    Report.fetchAll()
    .then(data => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', 'application/json');
      // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
      // res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
      // res.setHeader('Access-Control-Allow-Credentials', true);
      console.log(data);
      res.send(data);
    })
    .catch(err => {
      console.log(err);
    });
}
