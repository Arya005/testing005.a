const getDb = require('../util/database').getDb;
module.exports = class Report {
   
    static fetchAll() {
        const db = getDb();
        return db
        .collection('completion_report')
        .find()
        .limit(1000)
        .toArray()
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
    }
}
