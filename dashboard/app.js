// bootstap file
// const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const reportRoutes = require('./routes/report');
const mongoConnect = require('./util/database').mongoConnect;

const app = express();

app.use(bodyParser.json());

app.use(reportRoutes);



mongoConnect(() => {
    app.listen(3001);
});


